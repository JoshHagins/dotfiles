[user]
	name = Josh Hagins
	email = hagins.josh@gmail.com
[credential]
	helper = osxkeychain
[core]
	quotepath = false
	whitespace = fix,trailing-space,space-before-tab,cr-at-eol
	excludesfile = ~/.gitignore
	ignorecase = false
	editor=vim
[merge]
	tool = diffmerge
	verbosity = 2
[push]
	default = simple
[color]
	ui = true
[color "branch"]
	current = yellow reverse
	local = yellow
	remote = green
[color "diff"]
	# meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
	whitespace = red reverse
[color "status"]
	added = green
	changed = red
	untracked = cyan
[alias]
	ad = add
	st = status
	ci = commit
	br = branch
	co = checkout
	df = diff
	dc = diff --cached
	lg = log -p
	lc = log ORIG_HEAD.. --stat --no-merges
	ls = ls-files
	cl = clean
	rb = rebase
	pl = pull
	ps = push

	# Pull with rebase
	plr = pull --rebase

	# Git-gui
	gui = !sh -c '/usr/local/opt/git/libexec/git-core/git-gui'

	# Set upstream
	setup = !sh -c 'git branch -u origin/$1' -

	# Clean all untracked and ignored files and directories
	clean-all = clean -d -X
	cla = clean -d -X

	# Log with graph
	lol = log --graph --decorate --pretty=oneline --abbrev-commit
	lola = log --graph --decorate --pretty=oneline --abbrev-commit --all

	# Uncache files
	uc = reset HEAD

	# See new commits created by last command
	new = !sh -c 'git log $1@{1}..$1@{0} "$@"' -

	# Add only non-whitespace changes
	addnw = !sh -c 'git diff -w --no-color "$@" | git apply --cached --ignore-whitespace' -

	# Show files ignored by git:
	ign = ls-files -o -i --exclude-standard

	# Prune stale remote branches
	prune-all = !git remote | xargs -n 1 git remote prune

	# Undo any change in current branch
	undo = reset --hard

	# Common diff options
	ch = diff --name-status
	ds = diff --stat
	cs = diff --cached --stat

	# Assume file unchanged
	assume   = update-index --assume-unchanged
	unassume = update-index --no-assume-unchanged
	assumed  = "!git ls-files -v | grep ^h | cut -c 3-"

	# Take a snapshot of uncommitted work
	snapshot = !git stash save "snapshot: $(date)" && git stash apply "stash@{0}"
	snapshots = !git stash list --grep snapshot

	# Use version of file from current branch or merged branch
	ours   = "!f() { git checkout --ours $@ && git add $@; }; f"
	theirs = "!f() { git checkout --theirs $@ && git add $@; }; f"

[init]
	templatedir = ~/.git_template
[difftool "sourcetree"]
	cmd = /opt/homebrew-cask/Caskroom/diffmerge/4.2.0.697/DiffMerge.app/Contents/MacOS/DiffMerge --nosplash \"$LOCAL\" \"$REMOTE\"
	path = 
[mergetool "sourcetree"]
	cmd = /opt/homebrew-cask/Caskroom/diffmerge/4.2.0.697/DiffMerge.app/Contents/MacOS/DiffMerge --merge --result=\"$MERGED\" \"$LOCAL\" \"$BASE\" \"$REMOTE\"
	trustExitCode = true
[diff]
	algorithm = histogram
	tool = diffmerge
[help]
	autocorrect = 1
[hub]
	protocol = ssh
[mergetool "diffmerge"]
	cmd = diffmerge --nosplash --merge --result=$MERGED $LOCAL $BASE $REMOTE
	trustExitCode = true
[difftool "diffmerge"]
	cmd = diffmerge --nosplash $LOCAL $REMOTE
[mergetool]
	keepBackup = false
[github]
	user = jawshooah
