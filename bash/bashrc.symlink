#!/usr/bin/env bash

# Common aliases
alias ..='cd ..'
alias cd..='cd ..'
alias l='ll'
alias ls='ls -G'
alias gp='grep -Hn --color=auto'
alias fgp='fgrep -Hn --color=auto'
alias egp='egrep -Hn --color=auto'
alias vlc='open -a Vlc'
alias wha='which -a'

# Colorize command output
alias colorize='python -m colorize'

# Homebrew aliases
alias bc='brew cleanup'
alias bd='brew doctor'
alias be='brew edit'
alias bh='brew home'
alias bi='brew info'
alias bu='brew update'
alias bis='brew install'
alias bug='brew upgrade'
alias bus='brew uninstall'
alias budu='bu && bd && bug'

# Homebrew-cask aliases
alias bcc='brew cask cleanup'
alias bcd='brew cask doctor'
alias bce='brew cask edit'
alias bch='brew cask home'
alias bci='brew cask info'
alias bcu='brew cask update'
alias bcis='brew cask install'
alias bcug='brew cask upgrade'
alias bcus='brew cask uninstall'
alias bczp='brew cask zap'
alias buduc='budu && bc && bcc'

# Shorthand for colorized sdcv output
dict() {
  sdcv --color "$@" | less -r
}

export EDITOR='vim'
export GNUTERM='x11'
export JAVA_HOME="$(/usr/libexec/java_home -v '1.7*')"

NODE_PATH='/usr/local/lib/node_modules'
NODE_PATH="$HOME/node_modules:$NODE_PATH"
export NODE_PATH

CAML_LD_LIBRARY_PATH='/usr/local/lib/ocaml/stublibs'
CAML_LD_LIBRARY_PATH="$HOME/.opam/system/lib/stublibs:$CAML_LD_LIBRARY_PATH"
export CAML_LD_LIBRARY_PATH

OCAML_TOPLEVEL_PATH="$HOME/.opam/system/lib/toplevel"
export OCAML_TOPLEVEL_PATH

PATH='/sbin'
PATH="/usr/sbin:$PATH"
PATH="/usr/local/sbin:$PATH"
PATH="/bin:$PATH"
PATH="/usr/bin:$PATH"
PATH="/usr/X11/bin:$PATH"
PATH="/usr/local/bin:$PATH"
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
PATH="/usr/local/opt/ruby/bin:$PATH"
PATH="$HOME/.opam/system/bin:$PATH"
PATH="$HOME/.cabal/bin:$PATH"
PATH="$HOME/.rvm/bin:$PATH"
PATH="$HOME/bin:$PATH"
export PATH

MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
MANPATH="/usr/local/opt/gnu-sed/libexec/gnuman:$MANPATH"
export MANPATH

# Change ls alias if we are using GNU
ls --color -d . >/dev/null 2>&1 && alias ls='ls --color'
